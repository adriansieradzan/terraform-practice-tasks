terraform {
  backend "local" {
    path = "./terraform.tfstate"
  }

}
module "ec2_instances_demo" {
  source = "./ec2-instances"

  ec2_count     = 1
  environment   = "dev"
  instance_type = "t2.micro"
  ami           = "ami-0fe0b2cf0e1f25c8a"
}