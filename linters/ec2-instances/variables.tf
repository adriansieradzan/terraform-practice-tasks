variable "ec2_count" {
  description = "Count of ec2 instances"
  type        = number
  default     = 0
  validation {
    condition     = var.ec2_count <= 1 ? true : false
    error_message = "Not allowed more then one instance"
  }
}

variable "ami" {
  description = "AMI instance"
  type        = string
  default     = null

  validation {
    condition     = contains(["ami-0fe0b2cf0e1f25c8a", "ami-09d56f8956ab235b"], var.ami)
    error_message = "Not allowed AMI instance"
  }
}

variable "region" {
  description = "AWS region"
  type        = string
  default     = "eu-west-1"
  validation {
    condition     = contains(["eu-west-1", "us-east-1"], var.region)
    error_message = "Not allowed region"
  }
}

variable "instance_type" {
  description = "EC2 instance type"
  type        = string
  default     = null
  validation {
    condition     = contains(["t2.micro"], var.instance_type)
    error_message = "Not allowed instance type"
  }
}


variable "environment" {
  type        = string
  description = <<EOT
  The environment short name to use for the deployed resources.

  Options:
  - dev
  - prod

  EOT
  default     = "prod"

  validation {
    condition     = can(regex("^dev$|^prod$", var.environment))
    error_message = "Invalid environment."
  }
}
