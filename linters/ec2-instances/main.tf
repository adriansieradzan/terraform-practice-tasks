//VPC
resource "aws_vpc" "vpc" {
  cidr_block = "10.0.0.0/16"
  tags       = {
    Name = "terraform-lints-${var.environment}"
  }
}

data "aws_availability_zones" "available" {}

resource "aws_default_security_group" "default" {
  vpc_id = aws_vpc.vpc.id
}
resource "aws_subnet" "main" {
  count             = length(data.aws_availability_zones.available.names)
  vpc_id            = aws_vpc.vpc.id
  cidr_block        = "10.0.${count.index}.0/24"
  availability_zone = element(data.aws_availability_zones.available.names, count.index)

  tags = {
    Name = "public-${element(data.aws_availability_zones.available.names, count.index)}"
  }
}

resource "aws_security_group" "traffic" {
  name        = "terraform_security_group"
  description = "Terraform lints "
  vpc_id      = aws_vpc.vpc.id


  ingress {
    description = "Inbound on port 22 access"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = [
      "10.0.0.0/16"
    ]
  }

  # Allow outbound internet access.
  egress {
    description = "Allow outbound internet access."
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = [
      "1.2.3.4/32"
    ]
  }
  tags = {
    Name = "terraform-lints-${var.environment}"
  }
}

resource "aws_instance" "limited-ec2" {
  ami                    = var.ami
  instance_type          = var.instance_type
  subnet_id              = aws_subnet.main[0].id
  depends_on             = [aws_subnet.main, aws_security_group.traffic]
  security_groups        = [aws_security_group.traffic.id]
  count                  = var.ec2_count
  monitoring             = true
  ebs_optimized          = false // old family type instance
  vpc_security_group_ids = [aws_security_group.traffic.id]
  root_block_device {
    encrypted = true
  }
  metadata_options {
    http_endpoint = "enabled"
    http_tokens   = "required"
  }
  tags = {
    Name = "terraform-lints-${var.environment}"
  }
}