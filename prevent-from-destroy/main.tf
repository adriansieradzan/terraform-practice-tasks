terraform {
  backend "local" {
    path = "./terraform.tfstate"
  }

}

resource "aws_s3_bucket" "main" {
  bucket = "prevent-destroy-bucket"
  acl    = "private"
  lifecycle {
    prevent_destroy = true
  }
}