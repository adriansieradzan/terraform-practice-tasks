//terraform {
//  backend "s3" {
//    bucket = "terraform-bucket-lab-state-1"
//    key = "terraform/s3/terraform.tfstate"
//    region = "eu-west-1"
//    encrypt = true
//  }
//}

data "terraform_remote_state" "remote" {
  backend = "s3"
  config = {
    bucket = "terraform-bucket-lab-state-1"
    key = "terraform/s3/terraform.tfstate"
    region = "eu-west-1"
  }
}
output "number_resources" {
  description = "Number of resources"
  value = data.terraform_remote_state.remote.outputs
}


resource "aws_s3_bucket" "terraform_state" {
  bucket = "terraform-bucket-lab-state-1"
  versioning {
    enabled = true
  }

  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        sse_algorithm = "AES256"
      }
    }
  }
}

resource "aws_s3_bucket" "test_1" {
  bucket = "terraform-bucket-lab-state-1-test-1"

  tags = {
    Name = "My Test 1"
  }
}

resource "aws_s3_bucket_acl" "test_1" {
  bucket = aws_s3_bucket.test_1.id
  acl = "private"
}

resource "aws_s3_bucket" "test_2" {
  bucket = "terraform-bucket-lab-state-1-test-2"
  depends_on = [
    aws_s3_bucket.test_1]
  tags = {
    Name = "My Test 2"
  }
}

resource "aws_s3_bucket_acl" "test_2" {
  bucket = aws_s3_bucket.test_2.id
  acl = "private"
}



resource "aws_s3_bucket" "test_3" {
  bucket = "terraform-bucket-lab-state-1-test-3"
  depends_on = [
    aws_s3_bucket.test_1, aws_s3_bucket.test_2]
  tags = {
    Name = "My Test 3"
  }
}

resource "aws_s3_bucket_acl" "test_3" {
  bucket = aws_s3_bucket.test_3.id
  acl = "private"
}

