variable "ec2_enabled" {
  description = "Enable instance"
  type        = bool
  default     = true
}