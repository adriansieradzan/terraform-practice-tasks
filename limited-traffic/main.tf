terraform {
  backend "local" {
    path = "./terraform.tfstate"
  }

}

//VPC
resource "aws_vpc" "vpc" {
  cidr_block = "10.0.0.0/16"

  tags = {
    Name = "terraform-limited-traffic"
  }
}

resource "aws_flow_log" "example" {
  iam_role_arn    = "arn"
  log_destination = "log"
  traffic_type    = "ALL"
  vpc_id          = aws_vpc.vpc.id
}

data "aws_availability_zones" "available" {}

resource "aws_subnet" "main" {
  count             = length(data.aws_availability_zones.available.names)
  vpc_id            = aws_vpc.vpc.id
  cidr_block        = "10.0.${count.index}.0/24"
  availability_zone = element(data.aws_availability_zones.available.names, count.index)

  tags = {
    Name = "public-${element(data.aws_availability_zones.available.names, count.index)}"
  }
}

resource "aws_default_security_group" "default" {
  vpc_id = aws_vpc.vpc.id
}

resource "aws_security_group" "traffic" {
  name        = "terraform_security_group"
  description = "Terraform limited trafic"
  vpc_id      = aws_vpc.vpc.id


  ingress {
    description = "Inbound on port 22 access"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = [
      "10.0.0.0/16"
    ]
  }

  egress {
    description = "Allow outbound internet access."
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = [
      "0.0.0.0/0"
    ]
  }
  tags = {
    Name = "terraform-limited-traffic-security-group"
  }
}

resource "aws_instance" "limited-ec2" {
  ami                  = "ami-0fe0b2cf0e1f25c8a"
  instance_type        = "t2.micro"
  subnet_id            = aws_subnet.main[0].id
  depends_on           = [aws_subnet.main, aws_security_group.traffic]
  security_groups      = [aws_security_group.traffic.id]
  count                = var.ec2_enabled ? 1 : 0
  iam_instance_profile = "test"
  monitoring           = true
  ebs_optimized        = true
  root_block_device {
    encrypted = true
  }
  metadata_options {
    http_endpoint = "enabled"
    http_tokens   = "required"
  }
  tags = {
    Name = "terraform-limited-traffic-ec2"
  }
}